/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,ts}'],
  theme: {
    extend: {
      colors: {
        odb: {
          50: '#C7DEE235',
          100: '#e9ecee',
          200: '#c3d6db',
          300: '#6a98a6',
          400: '#418795',
          500: '#476C82',
          800: '#1f4053',
          inner: 'rgba(31, 64, 83, 0.5)',
          landing: 'rgba(31, 64, 83, 0.8)',
        },
        trans: {
          'white-50': 'rgba(255,255,255,0.5)',
          'white-90': 'rgba(255,255,255,0.9)',
        },
      },
      fontFamily: {
        sans: [
          'Open Sans',
          'ui-sans-serif',
          'system-ui',
          '-apple-system',
          'Segoe UI',
          'Helvetica Neue',
          'Arial',
          'Noto Sans',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji',
          'Segoe UI Symbol',
          'Noto Color Emoji',
        ],
        odb: ['Oswald', 'sans-serif'],
      },
      fontSize: {
        xxs: ['0.7rem', '0.75rem'],
        llg: ['1.15rem', '1.25rem'],
      },
      backgroundImage: {
        hero: "url('/assets/images/pisos-de-bancos.jpg')",
        'mortgage-h': "url('/assets/images/mortgage-home.jpg')",
        wwus: "url('/assets/images/trabaja-con-nosotros-bg.jpg')",
        profesional:
          "url('/assets/images/oficinas-oportunidades-de-bancos-1.jpeg')",
        construccion: "url('/assets/images/en-construccion.jpg')",
        landing: "url('/assets/images/oficinas-oportunidades-de-bancos.jpg')",
      },
      width: {
        fit: 'fit-content',
      },
      height: {
        gallery: '26rem',
        '2gallery': '32rem',
        fit: 'fit-content',
      },
      padding: {
        '1/7': '14.2857143%',
        '2/7': '28.5714286%',
        '3/7': '42.8571429%',
        '4/7': '57.1428571%',
        '5/7': '71.4285714%',
        '6/7': '85.7142857%',
      },
      cursor: {
        'zoom-in': 'zoom-in',
      },
    },
  },
  plugins: [],
};
