// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'odb-work-with-us',
    appId: '1:635191584027:web:3d33a52290391da4409fbe',
    databaseURL: 'https://odb-work-with-us-default-rtdb.europe-west1.firebasedatabase.app',
    storageBucket: 'odb-work-with-us.appspot.com',
    apiKey: 'AIzaSyBZJ5LgmaTCwUhup_cK9nQANrXAZfjGATA',
    authDomain: 'odb-work-with-us.firebaseapp.com',
    messagingSenderId: '635191584027',
  },
  production: false,
  apiBase: 'https://dev.api.bankoportunities.com',
  apiKey: 'KufeyVnR9v5hua6TKdE%k&LwA&SyUx',
  serverBase: 'http://localhost:4200/api',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
