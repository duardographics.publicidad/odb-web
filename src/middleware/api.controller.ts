import axios, { AxiosResponse } from 'axios';
import { Request, Response } from 'express';
import { Paginated } from 'src/app/shared/interfaces/api/paginated.model';
import { PropertyModel } from 'src/app/shared/interfaces/api/property.model';
import { environment } from 'src/environments/environment';

const headers = {
  SecurityKey: environment.apiKey,
  accept: 'application/json',
};

export const searchProperties = (req: Request, res: Response) => {
  axios({
    method: 'post',
    url: `${environment.apiBase}/Properties/Search`,
    headers: headers,
    data: req.body,
  })
    .then((response: AxiosResponse<Paginated<PropertyModel>>) => {
      res.send(response.data);
    })
    .catch();
};

export const getProperty = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Properties/Reference/${req.body.reference}`,
    headers: headers,
  })
    .then((response: AxiosResponse<PropertyModel>) => {
      res.send(response.data);
    })
    .catch(() => {
      res.send();
    });
};

export const sendEmail = (req: Request, res: Response) => {
  axios({
    method: 'post',
    url: `${environment.apiBase}/Email`,
    headers: headers,
    data: req.body,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const getProvincesMatches = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Provinces?text=${req.body.word}&countryId=0&pageNumber=1&pageSize=3&sortDesc=true`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const getMunicipalitiesMatches = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Municipalities?text=${req.body.word}&pageNumber=1&pageSize=10`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const getProvinceByID = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Provinces/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const getMunicipalitiesByID = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Municipalities/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const deleteProperty = (req: Request, res: Response) => {
  axios({
    method: 'delete',
    url: `${environment.apiBase}/Properties/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.status);
    })
    .catch();
};

export const disableProperty = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Properties/Disable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const enableProperty = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Properties/Enable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const getProvinces = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Provinces?pageSize=52`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const disableProvince = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Provinces/Disable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const enableProvince = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Provinces/Enable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const auditProvince = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Provinces/Audit/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};

export const disableMunicipality = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Municipalities/Disable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};
export const enableMunicipality = (req: Request, res: Response) => {
  axios({
    method: 'put',
    url: `${environment.apiBase}/Municipalities/Enable/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};
export const auditMunicipality = (req: Request, res: Response) => {
  axios({
    method: 'get',
    url: `${environment.apiBase}/Municipalities/Audit/${req.body.id}`,
    headers: headers,
  })
    .then((response: AxiosResponse) => {
      res.send(response.data);
    })
    .catch();
};
