import * as express from 'express';
import {
  searchProperties,
  getProperty,
  sendEmail,
  getProvincesMatches,
  getMunicipalitiesMatches,
  getProvinceByID,
  getMunicipalitiesByID,
  disableProperty,
  enableProperty,
  deleteProperty,
  auditMunicipality,
  disableMunicipality,
  enableMunicipality,
  auditProvince,
  disableProvince,
  enableProvince,
  getProvinces,
} from './api.controller';

const router = express.Router();

// Get properties by search
router.post('/searchProperties', searchProperties);

// Get properties by reference
router.post('/getProperty', getProperty);

// Send Email
router.post('/sendEmail', sendEmail);

// Get Provinces Matches
router.post('/getProvince', getProvincesMatches);

// Get Municipalities Matches
router.post('/getMunicipality', getMunicipalitiesMatches);

// Get Province by ID
router.post('getPRovinceByID', getProvinceByID);

// Get Municipality by ID
router.post('getMunicipalityByID', getMunicipalitiesByID);

// Get Disable Property by ID
router.post('/disableProperty', disableProperty);

// Get Enable Property by ID
router.post('/enableProperty', enableProperty);

// Get Delete Property by ID
router.post('/deleteProperty', deleteProperty);

router.post('/getProvinces', getProvinces);

router.post('/disableProvince', disableProvince);

router.post('/enableProvince', enableProvince);

router.post('/auditProvince', auditProvince);

router.post('/disableMunicipality', disableMunicipality);

router.post('/enableMunicipality', enableMunicipality);

router.post('/auditMunicipality', auditMunicipality);

export default router;
