import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { FranchiseeModel } from 'src/app/shared/interfaces/franchise-req.interface';
import { AuthService } from 'src/app/shared/services/shared/auth.service';
import { FirebaseService } from 'src/app/shared/services/shared/firebase.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-franchisee-requests',
  templateUrl: './franchisee-requests.component.html',
  styleUrls: ['./franchisee-requests.component.scss'],
})
export class FranchiseeRequestsComponent implements OnInit {
  tableData: FranchiseeModel[] = [];

  constructor(
    private fireService: FirebaseService,
    private authService: AuthService,
  ) {}

  logOut(): void {
    this.authService.logOut();
  }

  ngOnInit(): void {
    this.fireService
      .getAll()
      .pipe(take(1))
      .subscribe({
        next: data => {
          this.tableData = data;
        },
      });
  }

  exportExcel(): void {
    const element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Página 1');

    XLSX.writeFile(wb, 'solicitudes-franquiciados.xlsx');
  }
}
