import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseeRequestsComponent } from './franchisee-requests.component';

describe('FranchiseeRequestsComponent', () => {
  let component: FranchiseeRequestsComponent;
  let fixture: ComponentFixture<FranchiseeRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FranchiseeRequestsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FranchiseeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
