import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FranchiseeRequestsComponent } from './franchisee-requests.component';

const routes = [{ path: '', component: FranchiseeRequestsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [FranchiseeRequestsComponent],
  declarations: [FranchiseeRequestsComponent],
  providers: [],
})
export class FranchiseeRequestsModule {}
