import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchPropertyComponent } from './search-property.component';

const routes = [{ path: '', component: SearchPropertyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [SearchPropertyComponent],
  declarations: [SearchPropertyComponent],
  providers: [],
})
export class SearchPropertyModule {}
