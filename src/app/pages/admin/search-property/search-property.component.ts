import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, switchMap, take } from 'rxjs';
import { PropertyModel } from 'src/app/shared/interfaces/api/property.model';
import { PropertiesService } from 'src/app/shared/services/api/properties.service';
import { SearchService } from 'src/app/shared/services/features/search.service';
import { AuthService } from 'src/app/shared/services/shared/auth.service';

@Component({
  selector: 'app-search-property',
  templateUrl: './search-property.component.html',
  styleUrls: ['./search-property.component.scss'],
})
export class SearchPropertyComponent {
  currentProperty!: PropertyModel | undefined;

  constructor(
    private search: SearchService,
    private router: Router,
    private propertyService: PropertiesService,
    private authService: AuthService,
  ) {}

  logOut(): void {
    this.authService.logOut();
  }

  searchProperty(reference: string) {
    this.loadProperty(reference).subscribe({
      next: (property: PropertyModel) => {
        if (property) {
          property.oldReference = [
            ...property.reference.replace('ODB_', '').replace('-HP', ''),
          ]
            .reverse()
            .join('');

          this.currentProperty = property;
        } else {
          this.currentProperty = undefined;
        }
      },
    });
  }

  deleteProperty(id: number) {
    this.propertyService.deleteProperty(id).subscribe({
      next: () => {
        this.router.navigate(['admin-dashboard']);
        alert('Inmueble eliminado correctamente');
      },
    });
  }

  toggleProperty(id: number) {
    this.propertyService
      .disableProperty(id)
      .pipe(
        switchMap((res: string) => {
          if (res === 'Registro Ya Deshabilitado') {
            return this.propertyService.enableProperty(id);
          }
          return of('Registro Deshabilitado');
        }),
        take(1),
      )
      .subscribe({
        next: (res: string) => {
          if (res.toLowerCase().includes('deshabilitado')) {
            this.router.navigate(['admin-dashboard']);
          }
          alert(res.replace('Registro', 'Inmueble'));
        },
      });
  }

  loadProperty(reference: string): Observable<PropertyModel> {
    return this.search.getPropertyByRef(reference).pipe(take(1));
  }
}
