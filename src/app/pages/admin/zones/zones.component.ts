import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Province } from 'src/app/shared/interfaces/api/property.model';
import { AuthService } from 'src/app/shared/services/shared/auth.service';
import { environment } from 'src/environments/environment';
import { Paginated } from 'src/app/shared/interfaces/api/paginated.model';
import { map } from 'rxjs/operators';
import { Municipality } from 'src/app/shared/interfaces/property';

const headers = {
  SecurityKey: environment.apiKey,
  accept: 'application/json',
};

interface AuditModel {
  id: string;
  createdDate: string;
  createdBy: string;
  updatedDate: string;
  updatedBy: string;
  deletedDate?: string;
  deletedBy?: string;
  disabled: string;
}

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.scss'],
})
export class ZonesComponent implements OnInit {
  provinces: Province[] = [];
  provincesSize = 52;
  selectedZone!: Province | Municipality | null;
  zoneSelected = false;
  isEnabled = true;

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient,
  ) {}

  logOut(): void {
    this.authService.logOut();
  }

  ngOnInit(): void {
    const URL = '/api/getProvinces';
    this.httpClient
      .post(URL, {})
      .pipe(map(m => m as Paginated<Province>))
      .subscribe({
        next: (response: Paginated<Province>) => {
          this.provinces = response.list;
        },
      });
  }

  openZone(zone: Province, type: 'prov' | 'mun'): void {
    let URL = '';
    if (type === 'prov') {
      URL = '/api/auditProvince';
    } else {
      URL = '/api/auditMunicipality';
    }
    this.httpClient
      .post(URL, { id: zone.id })
      .pipe(map(r => r as AuditModel))
      .subscribe({
        next: (response: AuditModel) => {
          this.selectedZone = zone;
          this.zoneSelected = true;
          if (response.disabled) {
            this.isEnabled = false;
          } else {
            this.isEnabled = true;
          }
        },
      });
  }

  toggleZone(): void {
    const action = this.isEnabled ? 'disable' : 'enable';
    const URL = `/api/${action}Province`;
    this.httpClient.post(URL, { id: this.selectedZone?.id }).subscribe({
      next: () => {
        this.isEnabled = !this.isEnabled;
      },
    });
  }

  closeZone(): void {
    this.selectedZone = null;
    this.isEnabled = true;
    this.zoneSelected = false;
  }
}
