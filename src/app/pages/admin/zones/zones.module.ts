import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZonesComponent } from './zones.component';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';

const routes = [{ path: '', component: ZonesComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    MatDialogModule,
  ],
  declarations: [ZonesComponent],
  exports: [ZonesComponent],
})
export class ZonesModule {}
