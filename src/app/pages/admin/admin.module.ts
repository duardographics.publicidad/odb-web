import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';

const routes = [
  { path: '', component: AdminComponent },
  {
    path: 'requests',
    loadChildren: () =>
      import('./franchisee-requests/franchisee-requests.module').then(
        m => m.FranchiseeRequestsModule,
      ),
  },
  {
    path: 'properties',
    loadChildren: () =>
      import('./search-property/search-property.module').then(
        m => m.SearchPropertyModule,
      ),
  },
  {
    path: 'zones',
    loadChildren: () => import('./zones/zones.module').then(m => m.ZonesModule),
  },
];
@NgModule({
  declarations: [AdminComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ComponentsModule],
  exports: [AdminComponent],
})
export class AdminModule {}
