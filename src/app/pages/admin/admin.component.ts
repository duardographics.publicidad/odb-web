import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, take } from 'rxjs';
import { PropertyModel } from 'src/app/shared/interfaces/api/property.model';
import { SearchService } from 'src/app/shared/services/features/search.service';
import { AuthService } from 'src/app/shared/services/shared/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  currentProperty!: PropertyModel | undefined;
  unverifiedEmail = false;
  constructor(
    private search: SearchService,
    private router: Router,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.authService.getUser().subscribe(user => {
      this.authService.user = user;
      if (!user?.emailVerified) {
        this.unverifiedEmail = true;
        this.authService.checkVerification();
        this.authService.verifiedUser.subscribe(verified => {
          if (verified) {
            this.unverifiedEmail = false;
          }
        });
      }
    });
  }

  logOut(): void {
    this.authService.logOut();
  }

  searchProperty(reference: string) {
    this.loadProperty(reference).subscribe({
      next: (property: PropertyModel) => {
        if (property) {
          property.oldReference = [
            ...property.reference.replace('ODB_', '').replace('-HP', ''),
          ]
            .reverse()
            .join('');

          this.currentProperty = property;
        } else {
          this.currentProperty = undefined;
        }
      },
    });
  }

  loadProperty(reference: string): Observable<PropertyModel> {
    return this.search.getPropertyByRef(reference).pipe(take(1));
  }

  goToProperty(reference: string): void {
    this.router.navigate(['admin-dashboard/property/' + reference]);
  }

  resendEmail(): void {
    this.authService.resendVerificationEmail();
  }
}
