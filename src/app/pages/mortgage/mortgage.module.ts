import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MortgageComponent } from './mortgage.component';
import { MortgageRouting } from './morgage-routes.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { GsapDirectivesModule } from 'src/app/shared/directives/gsap/gsap-directives.module';

@NgModule({
  declarations: [MortgageComponent],
  imports: [
    CommonModule,
    RouterModule,
    MortgageRouting,
    ComponentsModule,
    GsapDirectivesModule,
  ],
  exports: [MortgageComponent],
})
export class MortgageModule {}
