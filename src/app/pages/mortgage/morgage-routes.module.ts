import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MortgageComponent } from './mortgage.component';

const routes: Routes = [{ path: '', component: MortgageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class MortgageRouting {}
