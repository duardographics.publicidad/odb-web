import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BlogComponent } from './blog.component';
import { BlogRouting } from './blog-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { GsapDirectivesModule } from 'src/app/shared/directives/gsap/gsap-directives.module';

@NgModule({
  declarations: [BlogComponent],
  imports: [
    CommonModule,
    RouterModule,
    BlogRouting,
    ComponentsModule,
    GsapDirectivesModule,
  ],
  exports: [BlogComponent],
})
export class BlogModule {}
