import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingMasterclassComponent } from './landing-masterclass.component';

const routes: Routes = [{ path: '', component: LandingMasterclassComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LandingMasterclassRouting {}
