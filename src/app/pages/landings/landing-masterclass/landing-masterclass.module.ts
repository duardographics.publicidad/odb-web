import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../shared/components/components.module';
import { RouterModule } from '@angular/router';
import { LandingMasterclassComponent } from './landing-masterclass.component';
import { LandingMasterclassRouting } from './landing-masterclass-routing.module';

@NgModule({
  declarations: [LandingMasterclassComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    LandingMasterclassRouting,
    RouterModule,
  ],
  exports: [LandingMasterclassComponent],
})
export class LandingMasterclassModule {}
