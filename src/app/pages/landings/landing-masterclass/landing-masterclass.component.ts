import { Component, ElementRef, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-masterclass',
  templateUrl: './landing-masterclass.component.html',
  styleUrls: ['./landing-masterclass.component.scss'],
})
export class LandingMasterclassComponent implements OnInit {
  constructor(private elementRef: ElementRef, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('Masterclass | Oportunidades de Bancos');
  }

  ngAfterViewInit() {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'https://event.webinarjam.com/register/rp5rltv/embed-button';
    this.elementRef.nativeElement.appendChild(s);
  }
}
