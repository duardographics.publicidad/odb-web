import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingMasterclassComponent } from './landing-masterclass.component';

describe('LandingMasterclassComponent', () => {
  let component: LandingMasterclassComponent;
  let fixture: ComponentFixture<LandingMasterclassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingMasterclassComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LandingMasterclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
