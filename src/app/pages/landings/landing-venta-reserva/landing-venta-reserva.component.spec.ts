import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingVentaReservaComponent } from './landing-venta-reserva.component';

describe('LandingVentaReservaComponent', () => {
  let component: LandingVentaReservaComponent;
  let fixture: ComponentFixture<LandingVentaReservaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingVentaReservaComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LandingVentaReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
