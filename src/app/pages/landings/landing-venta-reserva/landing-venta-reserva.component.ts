import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-venta-reserva',
  templateUrl: './landing-venta-reserva.component.html',
  styleUrls: ['./landing-venta-reserva.component.scss'],
})
export class LandingVentaReservaComponent implements OnInit {
  constructor(private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(
      'Oportunidades de bancos te ofrece la mayor red de activos inmobiliarios bancarios en toda España',
    );
  }
  partnersLinks: string[] = [
    'https://www.bbva.es/personas.html',
    'https://www.bancosantander.es/particulares',
    'https://www.haya.es/',
    'https://www.caixabank.es/particular/home/particulares_es.html',
    'https://www.alisedainmobiliaria.com/',
    'https://www.casaktua.com/',
    'https://www.bbva.es/personas.html',
    'https://www.escogecasa.es/',
    'https://www.bankinter.com/banca/inicio',
    'https://www.holapisos.com/',
    'https://www.retamarealestate.com/',
    'https://portalnow.com/es/',
    'https://www.bancamarch.es/es/',
    'https://www.nolon.es/es/',
    'https://uci.com/',
    'https://www.abanca.com/',
    'https://www.sareb.es/',
    'https://www.servihabitat.com/',
    'https://www.ruralvia.com/',
    'https://digloservicer.com/',
    'https://www.solvia.es/',
    'https://www.liberbank.es/',
    'https://www.altamirainmuebles.com/venta-pisos',
    'https://www.cajamar.es/es/comun/',
    'https://www.caixabank.es/particular/general/caixabank-bankia.html',
    'https://www.unicajabanco.es/es/particulares',
    'https://www.unicajabanco.es/es/particulares',
    'https://www.neinorhomes.com/',
    'https://www.ibercaja.es/',
    'https://www.goldmansachs.com/worldwide/spain/',
    'https://www.caixabank.es/particular/home/particulares_es.html',
    'https://www.alisedainmobiliaria.com/',
    'https://www.anticipa.com/corp/',
    'https://www.hipoges.com/',
  ];
}
