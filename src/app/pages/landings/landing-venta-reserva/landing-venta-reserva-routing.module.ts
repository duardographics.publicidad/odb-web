import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingVentaReservaComponent } from './landing-venta-reserva.component';

const routes: Routes = [{ path: '', component: LandingVentaReservaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LandingVentaReservaRouting {}
