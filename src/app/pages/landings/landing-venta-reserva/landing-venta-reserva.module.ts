import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../shared/components/components.module';
import { RouterModule } from '@angular/router';
import { LandingVentaReservaComponent } from './landing-venta-reserva.component';
import { LandingVentaReservaRouting } from './landing-venta-reserva-routing.module';

@NgModule({
  declarations: [LandingVentaReservaComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    LandingVentaReservaRouting,
    RouterModule,
  ],
  exports: [LandingVentaReservaComponent],
})
export class LandingVentaReservaModule {}
