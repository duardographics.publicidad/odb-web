import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../shared/components/components.module';
import { RouterModule } from '@angular/router';
import { LandingGraciasComponent } from './landing-gracias.component';
import { LandingGraciasRouting } from './landing-gracias-routing.module';

@NgModule({
  declarations: [LandingGraciasComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    LandingGraciasRouting,
    RouterModule,
  ],
  exports: [LandingGraciasComponent],
})
export class LandingGraciasModule {}
