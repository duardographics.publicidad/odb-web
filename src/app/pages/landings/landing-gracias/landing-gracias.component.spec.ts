import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingGraciasComponent } from './landing-gracias.component';

describe('LandingGraciasComponent', () => {
  let component: LandingGraciasComponent;
  let fixture: ComponentFixture<LandingGraciasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingGraciasComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LandingGraciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
