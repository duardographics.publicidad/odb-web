import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingGraciasComponent } from './landing-gracias.component';

const routes: Routes = [{ path: '', component: LandingGraciasComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LandingGraciasRouting {}
