import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
declare let gtag: any;
declare let fbq: any;

@Component({
  selector: 'app-landing-gracias',
  templateUrl: './landing-gracias.component.html',
  styleUrls: ['./landing-gracias.component.scss'],
})
export class LandingGraciasComponent {
  constructor(private router: Router) {
    router.events.subscribe((y: any) => {
      if (y instanceof NavigationEnd) {
        gtag('config', 'G-H4YGHS4GKH', { page_path: y.url });
        fbq('track', 'PageView');
      }
    });
  }
}
