import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  Inject,
  InjectionToken,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-reserva',
  templateUrl: './landing-reserva.component.html',
  styleUrls: ['./landing-reserva.component.scss'],
})
export class LandingReservaComponent implements OnInit {
  constructor(
    private title: Title,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {}

  isBrowser = false;

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.isBrowser = true;
    }
    this.title.setTitle('Reserva | Oportunidades de Bancos');
  }
}
