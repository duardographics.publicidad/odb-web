import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../shared/components/components.module';
import { LandingReservaRouting } from './landing-reserva-routing.module';
import { RouterModule } from '@angular/router';
import { LandingReservaComponent } from './landing-reserva.component';

@NgModule({
  declarations: [LandingReservaComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    LandingReservaRouting,
    RouterModule,
  ],
  exports: [LandingReservaComponent],
})
export class LandingReservaModule {}
