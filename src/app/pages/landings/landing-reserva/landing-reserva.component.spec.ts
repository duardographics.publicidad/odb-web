import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingReservaComponent } from './landing-reserva.component';

describe('LandingReservaComponent', () => {
  let component: LandingReservaComponent;
  let fixture: ComponentFixture<LandingReservaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingReservaComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LandingReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
