import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-replay-masterclass',
  templateUrl: './landing-replay-masterclass.component.html',
  styleUrls: ['./landing-replay-masterclass.component.scss'],
})
export class LandingReplayMasterclassComponent implements OnInit {
  constructor(private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('Masterclass | Oportunidades de Bancos');
  }
}
