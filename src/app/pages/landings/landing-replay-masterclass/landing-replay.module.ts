import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../shared/components/components.module';
import { RouterModule } from '@angular/router';
import { LandingReplayMasterclassComponent } from './landing-replay-masterclass.component';
import { LandingReplayRouting } from './landing-replay-routing.module';

@NgModule({
  declarations: [LandingReplayMasterclassComponent],
  imports: [CommonModule, ComponentsModule, LandingReplayRouting, RouterModule],
  exports: [LandingReplayMasterclassComponent],
})
export class LAndingReplayModule {}
