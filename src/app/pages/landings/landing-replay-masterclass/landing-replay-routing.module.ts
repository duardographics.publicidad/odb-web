import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingReplayMasterclassComponent } from './landing-replay-masterclass.component';

const routes: Routes = [
  { path: '', component: LandingReplayMasterclassComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LandingReplayRouting {}
