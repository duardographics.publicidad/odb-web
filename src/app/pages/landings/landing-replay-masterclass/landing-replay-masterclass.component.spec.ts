import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingReplayMasterclassComponent } from './landing-replay-masterclass.component';

describe('LandingReplayMasterclassComponent', () => {
  let component: LandingReplayMasterclassComponent;
  let fixture: ComponentFixture<LandingReplayMasterclassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LandingReplayMasterclassComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LandingReplayMasterclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
