import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { COMMON_SEARCHES } from 'src/app/shared/data/constant-data/common-searches';
import { PARTNERS_LINKS } from 'src/app/shared/data/constant-data/partners-links';
import { SearchService } from 'src/app/shared/services/features/search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  partners = PARTNERS_LINKS;
  commonSearches = COMMON_SEARCHES;

  constructor(private searchService: SearchService, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle(
      'Oportunidades de bancos te ofrece la mayor red de activos inmobiliarios bancarios en toda España.',
    );
  }
}
