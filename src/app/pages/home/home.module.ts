import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { HomeRouting } from './home-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { GsapDirectivesModule } from 'src/app/shared/directives/gsap/gsap-directives.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule,
    HomeRouting,
    GsapDirectivesModule,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
