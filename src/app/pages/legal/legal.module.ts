import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LegalComponent } from './legal.component';
import { LegalRouting } from './legal-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [LegalComponent],
  imports: [CommonModule, RouterModule, LegalRouting, ComponentsModule],
  exports: [LegalComponent],
})
export class LegalModule {}
