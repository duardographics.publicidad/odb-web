import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CookiesComponent } from './cookies.component';
import { CookiesRouting } from './cookies-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [CookiesComponent],
  imports: [CommonModule, RouterModule, CookiesRouting, ComponentsModule],
  exports: [CookiesComponent],
})
export class CookiesModule {}
