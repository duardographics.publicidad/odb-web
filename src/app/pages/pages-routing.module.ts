import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { AdminGuard } from '../shared/guards/admin.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['']);

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'comprar',
        loadChildren: () =>
          import('./oportunities/oportunities.module').then(
            m => m.OportunitiesModule,
          ),
      },
      {
        path: 'contactanos',
        loadChildren: () =>
          import('./contact/contact.module').then(m => m.ContactModule),
      },
      {
        path: 'calcular-hipoteca',
        loadChildren: () =>
          import('./mortgage/mortgage.module').then(m => m.MortgageModule),
      },
      {
        path: 'inmueble',
        loadChildren: () =>
          import('./property/property.module').then(m => m.PropertyModule),
      },
      {
        path: 'trabaja-con-nosotros',
        loadChildren: () =>
          import('./work-with-us/work-with-us.module').then(
            m => m.WorkWithUsModule,
          ),
      },
      {
        path: 'blog',
        loadChildren: () =>
          import('./blog/blog.module').then(m => m.BlogModule),
      },
      {
        path: 'acceso-profesionales',
        loadChildren: () =>
          import('./accesos/accesos.module').then(m => m.AccesosModule),
      },
      {
        path: 'politica-de-privacidad',
        loadChildren: () =>
          import('./privacy/privacy.module').then(m => m.PrivacyModule),
      },
      {
        path: 'politica-de-cookies',
        loadChildren: () =>
          import('./cookies/cookies.module').then(m => m.CookiesModule),
      },
      {
        path: 'aviso-legal',
        loadChildren: () =>
          import('./legal/legal.module').then(m => m.LegalModule),
      },
      {
        path: 'reserva',
        loadChildren: () =>
          import('./landings/landing-reserva/landing-reserva.module').then(
            m => m.LandingReservaModule,
          ),
      },
      {
        path: 'venta-reserva',
        loadChildren: () =>
          import(
            './landings/landing-venta-reserva/landing-venta-reserva.module'
          ).then(m => m.LandingVentaReservaModule),
      },
      {
        path: 'gracias-por-reservar',
        loadChildren: () =>
          import('./landings/landing-gracias/landing-gracias.module').then(
            m => m.LandingGraciasModule,
          ),
      },
      {
        path: 'masterclass',
        loadChildren: () =>
          import(
            './landings/landing-masterclass/landing-masterclass.module'
          ).then(m => m.LandingMasterclassModule),
      },
      {
        path: 'replay-masterclass',
        loadChildren: () =>
          import(
            './landings/landing-replay-masterclass/landing-replay.module'
          ).then(m => m.LAndingReplayModule),
      },
      { path: 'replay', redirectTo: 'replay-masterclass' },
      {
        path: 'admin-dashboard',
        loadChildren: () =>
          import('./admin/admin.module').then(m => m.AdminModule),
        canActivate: [AuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToLogin },
      },
      {
        path: 'login',
        loadChildren: () =>
          import('./accesos/login/login.module').then(m => m.LoginModule),
      },
      {
        path: 'odb-sign-up',
        loadChildren: () =>
          import('./accesos/sign-up/sign-up.module').then(m => m.SignUpModule),
      },
      { path: '**', redirectTo: '' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PagesRoutingModule {}
