import { Component, Input } from '@angular/core';
import { Property } from 'src/app/shared/interfaces/property';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent {
  @Input() property!: Property;
}
