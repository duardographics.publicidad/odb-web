import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../shared/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PropertyComponent } from './property.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { GalleryComponent } from './gallery/gallery.component';
import { DescriptionComponent } from './description/description.component';
import { FeaturesComponent } from './features/features.component';
import { TitleComponent } from './title/title.component';
import { PriceComponent } from './price/price.component';
import { IconsComponent } from './icons/icons.component';
import { TagsComponent } from './tags/tags.component';
import { MapComponent } from './map/map.component';
import { MortgageComponent } from './mortgage/mortgage.component';
import { PropertyRoutingModule } from './property-routing.module';
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  declarations: [
    PropertyComponent,
    BreadcrumbComponent,
    GalleryComponent,
    DescriptionComponent,
    FeaturesComponent,
    TitleComponent,
    PriceComponent,
    IconsComponent,
    TagsComponent,
    MapComponent,
    MortgageComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    PropertyRoutingModule,
    RecaptchaModule,
  ],
  exports: [
    PropertyComponent,
    BreadcrumbComponent,
    GalleryComponent,
    DescriptionComponent,
    FeaturesComponent,
    TitleComponent,
    PriceComponent,
    IconsComponent,
    TagsComponent,
    MapComponent,
    MortgageComponent,
  ],
})
export class PropertyModule {}
