import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyComponent } from './property.component';

const routes: Routes = [
  {
    path: ':id',
    component: PropertyComponent,
  },
  {
    path: '',
    redirectTo: '/comprar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PropertyRoutingModule {}
