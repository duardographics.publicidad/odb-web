import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs';
import { Property } from 'src/app/shared/interfaces/property';
import { PropertyService } from 'src/app/shared/services/shared/property.service';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss'],
})
export class PropertyComponent implements OnInit {
  reference!: string;
  property!: Property;
  constructor(
    private actRoute: ActivatedRoute,
    private route: Router,
    public propertyService: PropertyService,
    private title: Title,
  ) {
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
    this.reference = this.actRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    if (this.reference) {
      this.propertyService
        .getProperty(this.reference)
        .pipe(take(1))
        .subscribe({
          next: (res: Property) => {
            if (res.id) {
              this.property = res;
              this.title.setTitle(
                `${this.property.propertySubtype.name} en venta en ${this.property.municipality.name} - ${this.property.price}€`,
              );
            } else {
              this.route.navigate(['comprar']);
            }
          },
        });
    } else {
      this.route.navigate(['comprar']);
    }
  }
}
