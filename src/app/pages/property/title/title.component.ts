import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
})
export class TitleComponent {
  @Input() title: {
    address: string;
    zipCode: string;
    municipality: string;
    subType: string;
    province?: string;
  } = { address: '', zipCode: '', municipality: '', subType: '' };
}
