import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { circle, Map, tileLayer } from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit, OnInit {
  @Input() coordinates: number[] = [];
  @Input() street: string[] = [];

  map!: any;

  ngOnInit(): void {
    if (this.map) {
      this.map.remove();
    }
  }

  ngAfterViewInit(): void {
    if (this.coordinates[0]) {
      this.map = new Map('location').setView(
        [this.coordinates[0], this.coordinates[1]],
        15,
      );
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap',
      }).addTo(this.map);

      circle([this.coordinates[0], this.coordinates[1]], {
        color: '#418795',
        fillColor: '#418795',
        fillOpacity: 0.5,
        radius: 300,
      }).addTo(this.map);
    }
  }
}
