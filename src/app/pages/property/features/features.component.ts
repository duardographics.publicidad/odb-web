import { Component, Input, OnInit } from '@angular/core';
import { MainFeatures } from 'src/app/shared/interfaces/api/property.model';
import { Property } from 'src/app/shared/interfaces/property';
import { FeaturesService } from 'src/app/shared/services/features/features.service';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss'],
})
export class FeaturesComponent implements OnInit {
  @Input() property!: Property;
  @Input() mainFeatures!: MainFeatures;

  features = {};

  constructor(private featuresService: FeaturesService) {}

  ngOnInit(): void {
    this.features = this.featuresService.getFeatures(this.property);
    this.features = { ...this.features, ...this.mainFeatures };
  }
}
