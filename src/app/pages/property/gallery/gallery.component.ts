import { Component, Input } from '@angular/core';
import { PropertyMultimedia } from 'src/app/shared/interfaces/property';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent {
  @Input() media!: PropertyMultimedia[];

  galOpen = false;

  scrollTopGal(gallery: HTMLElement) {
    if (gallery.scrollTop != 0) {
      gallery.scrollTop = 0;
    }
  }

  toggleGallery = (gallery: HTMLElement): void => {
    this.scrollTopGal(gallery);
    this.galOpen = !this.galOpen;
  };
  openGallery = (gallery: HTMLElement): void => {
    this.scrollTopGal(gallery);
    this.galOpen = true;
  };
}
