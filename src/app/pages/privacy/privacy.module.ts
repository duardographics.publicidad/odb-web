import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyComponent } from './privacy.component';
import { PrivacyRouting } from './privacy-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [PrivacyComponent],
  imports: [CommonModule, PrivacyRouting, ComponentsModule],
  exports: [PrivacyComponent],
})
export class PrivacyModule {}
