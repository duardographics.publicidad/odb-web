import { Component, OnInit } from '@angular/core';
import { UserCredential } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/shared/services/shared/auth.service';
// import { AuthService } from 'src/app/shared/services/shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  destroy$ = new Subject();
  loginError = false;
  noEmail = false;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.noEmail = false;
      this.authService
        .login({
          email: this.loginForm.value.email,
          password: this.loginForm.value.password,
        })
        .then((res: UserCredential) => {
          this.authService.user = res.user;
          window.localStorage.setItem('--lg', new Date().getDate().toString());
          this.router.navigate(['/admin-dashboard']);
        })
        .catch(() => (this.loginError = true));
    }
  }

  resetPassword() {
    if (this.loginForm.value.email) {
      this.noEmail = false;
      this.authService.resetPassword(this.loginForm.value.email);
    } else {
      this.noEmail = true;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }
}
