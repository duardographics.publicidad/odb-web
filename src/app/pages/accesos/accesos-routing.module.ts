import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccesosComponent } from './accesos.component';

const routes: Routes = [{ path: '', component: AccesosComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AccesosRouting {}
