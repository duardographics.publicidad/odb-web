import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/shared/services/shared/auth.service';
// import { AuthService } from 'src/app/shared/services/shared/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit, OnDestroy {
  loginForm!: FormGroup;
  alreadyInUse = false;
  loading = false;
  destroy$ = new Subject();
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      if (!this.loginForm.value.email.includes('oportunidadesdebancos')) {
        alert(
          'Por favor introduce un correo electrónico perteneciente a la organización',
        );
        return;
      }
      this.loading = true;
      this.authService
        .register({
          email: this.loginForm.value.email,
          password: this.loginForm.value.password,
        })
        .then(() => {
          this.alreadyInUse = false;
          this.loading = false;
          this.authService.checkVerification();
          this.router.navigate(['login']);
        })
        .catch(() => {
          this.alreadyInUse = true;
          this.loading = false;
        });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }
}
