import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AccesosComponent } from './accesos.component';
import { AccesosRouting } from './accesos-routing.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [AccesosComponent],
  imports: [CommonModule, RouterModule, AccesosRouting, ComponentsModule],
  exports: [AccesosComponent],
})
export class AccesosModule {}
