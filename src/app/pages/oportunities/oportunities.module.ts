import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OportunitiesComponent } from './oportunities.component';
import { OportunitiesRouting } from './oportunities-routing.module';
import { FilterModule } from './filter/filter.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { GsapDirectivesModule } from 'src/app/shared/directives/gsap/gsap-directives.module';

@NgModule({
  declarations: [OportunitiesComponent],
  imports: [
    CommonModule,
    RouterModule,
    OportunitiesRouting,
    FilterModule,
    ComponentsModule,
    GsapDirectivesModule,
  ],
  exports: [OportunitiesComponent],
})
export class OportunitiesModule {}
