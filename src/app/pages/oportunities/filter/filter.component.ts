import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  EventEmitter,
  Inject,
  InjectionToken,
  Input,
  OnInit,
  Output,
  PLATFORM_ID,
} from '@angular/core';
import { Params, Router } from '@angular/router';
import { PROPERTY_SUBTYPES } from 'src/app/shared/data/constant-data/property-subtypes';
import { PROPERTY_TYPES } from 'src/app/shared/data/constant-data/property-types';
import { Paginated } from 'src/app/shared/interfaces/api/paginated.model';
import {
  Municipality,
  PropertySubtype,
  Province,
} from 'src/app/shared/interfaces/api/property.model';
import { SearchService } from 'src/app/shared/services/features/search.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  @Input() queryParams!: Params;
  @Input() selectedTypes: number[] = [];
  @Input() subTypesSelected: number[] = [];
  types = PROPERTY_TYPES;
  subtypes = PROPERTY_SUBTYPES;

  dropedDown?: string;
  openned = false;
  subTypesToShow: PropertySubtype[] = [];
  priceFrom?: string = '0';
  priceTo?: string = '0';
  fromSQM?: string = '0';
  toSQM?: string = '0';
  rooms = 0;
  baths = 0;
  surfaceFrom?: number;
  surfaceTo?: number;
  currentSearch = '';
  @Output() returnCurrentSearch = new EventEmitter<string>();

  constructor(
    private router: Router,
    private search: SearchService,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {}

  returnLocation = () => {
    this.returnCurrentSearch.emit(this.currentSearch);
  };

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      window.addEventListener('click', (e: any) => {
        if (e.path && e.path[0]) {
          const path = e.path[0].nodeName;
          if (path === 'SECTION' || path === 'MAIN') {
            this.clearDroped();
          }
        }
      });
    }

    if (this.queryParams['priceFrom']) {
      this.priceFrom = this.queryParams['priceFrom'];
    }
    if (this.queryParams['priceTo']) {
      this.priceTo = this.queryParams['priceTo'];
    }
    if (this.queryParams['fromSQM']) {
      this.fromSQM = this.queryParams['fromSQM'];
    }
    if (this.queryParams['toSQM']) {
      this.toSQM = this.queryParams['toSQM'];
    }
    if (this.queryParams['rooms']) {
      this.rooms = Number(this.queryParams['rooms']);
    }
    if (this.queryParams['baths']) {
      this.baths = Number(this.queryParams['baths']);
    }
    if (this.queryParams['provinceID']) {
      this.search
        .getProvinceByID(Number(this.queryParams['provinceID']))
        .subscribe({
          next: (res: Province) => {
            this.currentSearch = res.name;
          },
        });
    }
    if (this.queryParams['municipalityID']) {
      this.search
        .getMunicipalitiesByID(Number(this.queryParams['municipalityID']))
        .subscribe({
          next: (res: Municipality) => {
            this.currentSearch = res.name;
          },
        });
    }
    this.loadSubtypes();
    this.returnLocation();
  }

  toggleFilter() {
    this.openned = !this.openned;
  }

  dropDown(el: string) {
    if (el === this.dropedDown) {
      this.clearDroped();
    } else {
      this.clearDroped();
      this.dropedDown = el;
    }
  }

  clearDroped() {
    this.dropedDown = undefined;
  }

  selectTypes(value: number) {
    if (this.selectedTypes.includes(value)) {
      this.selectedTypes = this.selectedTypes.filter((x: number) => {
        return x != value;
      });
    } else {
      this.selectedTypes.push(value);
    }
    this.loadSubtypes();
  }
  clearTypes() {
    this.selectedTypes = [];
    delete this.queryParams['type'];

    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  }
  selectSubTypes(value: number) {
    if (this.subTypesSelected.includes(value)) {
      this.subTypesSelected = this.subTypesSelected.filter((x: number) => {
        return x != value;
      });
    } else {
      this.subTypesSelected.push(value);
    }
  }
  loadSubtypes() {
    if (this.selectedTypes.length > 0) {
      for (const selectedType of this.selectedTypes) {
        this.subTypesToShow = this.subtypes.filter((x: PropertySubtype) => {
          return x.propertyType?.id === selectedType;
        });
      }
    }
  }

  applyFilter() {
    this.queryParams['page'] = 1;

    this.checkTypes();
    this.checkPrices();
    this.checkSurface();
    this.checkRooms();
    this.checkBaths();
    this.checkSubtypes();

    this.router.navigate(['/comprar'], { queryParams: this.queryParams });

    this.clearDroped();
  }
  checkTypes() {
    const types = this.selectedTypes.join(',');
    if (types != this.queryParams['type']) {
      if (types === '') {
        delete this.queryParams['type'];
        delete this.queryParams['subtype'];
        this.subTypesSelected = [];
      } else {
        this.queryParams['type'] = this.selectedTypes.join(',');
        if (!this.selectedTypes.includes(1)) {
          this.rooms = 0;
          this.baths = 0;
        }
      }
    }
  }
  checkSubtypes() {
    const types = this.subTypesSelected.join(',');
    if (types != this.queryParams['subtype']) {
      if (types === '') {
        delete this.queryParams['subtype'];
      } else {
        this.queryParams['subtype'] = this.subTypesSelected.join(',');
      }
    }
  }
  checkPrices() {
    const priceFrom = Number(this.priceFrom);
    const priceTo = Number(this.priceTo);
    if (priceFrom != this.queryParams['priceFrom']) {
      if (priceFrom === 0) {
        delete this.queryParams['priceFrom'];
      } else {
        this.queryParams['priceFrom'] = priceFrom;
      }
    }
    if (priceTo != this.queryParams['priceTo']) {
      if (priceTo === 0) {
        delete this.queryParams['priceTo'];
      } else {
        this.queryParams['priceTo'] = priceTo;
      }
    }
    if (priceTo != 0 && priceTo < priceFrom) {
      this.priceTo = this.priceFrom;
      this.queryParams['priceTo'] = this.priceTo;
    }
  }
  checkSurface() {
    const fromSQM = Number(this.fromSQM);
    const toSQM = Number(this.toSQM);
    if (fromSQM != this.queryParams['fromSQM']) {
      if (fromSQM === 0) {
        delete this.queryParams['fromSQM'];
      } else {
        this.queryParams['fromSQM'] = fromSQM;
      }
    }
    if (toSQM != this.queryParams['toSQM']) {
      if (toSQM === 0) {
        delete this.queryParams['toSQM'];
      } else {
        this.queryParams['toSQM'] = toSQM;
      }
    }
    if (toSQM != 0 && toSQM < fromSQM) {
      this.toSQM = this.fromSQM;
      this.queryParams['toSQM'] = this.toSQM;
    }
  }
  checkRooms() {
    if (this.rooms != this.queryParams['rooms']) {
      if (this.rooms === 0) {
        delete this.queryParams['rooms'];
      } else {
        this.queryParams['rooms'] = this.rooms;
      }
    }
  }
  checkBaths() {
    if (this.baths != this.queryParams['baths']) {
      if (this.baths === 0) {
        delete this.queryParams['baths'];
      } else {
        this.queryParams['baths'] = this.baths;
      }
    }
  }

  clearPrices() {
    delete this.queryParams['priceFrom'];
    delete this.queryParams['priceTo'];

    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  }
  clearSurface() {
    delete this.queryParams['fromSQM'];
    delete this.queryParams['toSQM'];

    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  }

  provinces: Province[] = [];
  municipalities: Municipality[] = [];
  searching = false;

  clearSearch = (): void => {
    this.provinces = [];
    this.municipalities = [];
  };

  searchByWords = (inputSearch: string): void => {
    this.clearSearch();
    if (inputSearch.length >= 3) {
      this.getResults(inputSearch);
    } else {
      this.clearSearch();
    }
  };

  getResults = (inputSearch: string) => {
    this.searching = true;
    this.search.getMunicipalitiesMatches(inputSearch).subscribe({
      next: (x: Paginated<Municipality>) => {
        this.municipalities = x.list;
        this.searching = false;
      },
    });
    this.search.getProvincesMatches(inputSearch).subscribe({
      next: (x: Paginated<Province>) => {
        this.provinces = x.list;
        this.searching = false;
      },
    });
  };

  clickOnResults(type: string, id: string | number) {
    delete this.queryParams['provinceID'];
    delete this.queryParams['municipalityID'];
    this.queryParams['page'] = 1;
    if (type === 'province') {
      this.queryParams['provinceID'] = id;
    } else if (type === 'municipality') {
      this.queryParams['municipalityID'] = id;
    }
    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  }

  buyByLocation = () => {
    delete this.queryParams['provinceID'];
    delete this.queryParams['municipalityID'];
    this.queryParams['page'] = 1;
    if (this.provinces.length > 0) {
      this.queryParams['provinceID'] = this.provinces[0].id;
    } else if (this.municipalities.length > 0) {
      this.queryParams['municipalityID'] = this.municipalities[0].id;
    }
    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  };

  clearLocation() {
    delete this.queryParams['provinceID'];
    delete this.queryParams['municipalityID'];
    this.queryParams['page'] = 1;
    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  }
}
