import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  Inject,
  InjectionToken,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Paginated } from 'src/app/shared/interfaces/api/paginated.model';
import { PropertyModel } from 'src/app/shared/interfaces/api/property.model';
import { FilterInterface } from 'src/app/shared/interfaces/filter.interface';
import { PropertiesService } from 'src/app/shared/services/api/properties.service';
import { OportunitiesPageService } from 'src/app/shared/services/features/oportunities-page.service';

@Component({
  selector: 'app-oportunities',
  templateUrl: './oportunities.component.html',
  styleUrls: ['./oportunities.component.scss'],
})
export class OportunitiesComponent implements OnInit {
  queryParams: Params = {};
  propertyTypes: number[] = [];
  propertySubtypes: number[] = [];
  results: Array<any> = [];
  noResults = false;
  totalPages = 1;
  pageSize = 1;
  pageFrom = 1;
  totalProperties = 1;
  currentSearch = '';

  oportunitiesRequest: FilterInterface;

  getCurrentSearch(currentSearch: any) {
    this.currentSearch = currentSearch;
  }

  constructor(
    private activeRoute: ActivatedRoute,
    private propService: PropertiesService,
    private router: Router,
    public opPage: OportunitiesPageService,
    private title: Title,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.oportunitiesRequest = { pageNumber: 1, pageSize: 30, sortDesc: true };
    this.activeRoute.queryParams.subscribe(res => {
      this.queryParams = { ...res };
      this.oportunitiesRequest.pageNumber = res['page']
        ? Number(res['page'])
        : 1;

      if (res['provinceID']) {
        this.oportunitiesRequest.provinceId = res['provinceID'];
      }
      if (res['municipalityID']) {
        this.oportunitiesRequest.municipalityID = res['municipalityID'];
      }
      if (res['type']) {
        this.propertyTypes = res['type']
          .split(',')
          .map((n: string) => Number(n));
        this.oportunitiesRequest.hasPropertyTypes = this.propertyTypes.map(
          (x: number) => {
            return { id: x };
          },
        );
      }
      if (res['subtype']) {
        this.propertySubtypes = res['subtype']
          .split(',')
          .map((n: string) => Number(n));
        this.propertySubtypes;
        this.oportunitiesRequest.hasPropertySubTypes =
          this.propertySubtypes.map((x: number) => {
            return { id: x };
          });
      }
      if (res['priceFrom']) {
        this.oportunitiesRequest.fromPrice = Number(res['priceFrom']);
      }
      if (res['priceTo'] && Number(res['priceTo']) > 0) {
        this.oportunitiesRequest.toPrice = Number(res['priceTo']);
      }
      if (res['fromSQM']) {
        this.oportunitiesRequest.fromSQM = Number(res['fromSQM']);
      }
      if (res['toSQM'] && Number(res['toSQM']) > 0) {
        this.oportunitiesRequest.toSQM = Number(res['toSQM']);
      }
      if (res['rooms'] && Number(res['rooms']) > 0) {
        this.oportunitiesRequest.fromRoomsNumber = Number(res['rooms']);
      }
      if (res['baths'] && Number(res['baths']) > 0) {
        this.oportunitiesRequest.fromBathroomsNumber = Number(res['baths']);
      }
    });
  }
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.title.setTitle(
        'Pisos de bancos, casas, oficinas, terrenos, y mucho más... Todo en un solo lugar | Oportunidades de Bancos',
      );
      this.propService
        .loadOportunitiesPage(this.oportunitiesRequest)
        .subscribe((res: Paginated<PropertyModel>) => {
          if (res.list.length === 0) {
            this.noResults = true;
          } else {
            this.noResults = false;
          }
          this.results = res.list;
          this.totalPages = Math.ceil(res.totalRows / res.pageSize);
          this.totalProperties = res.totalRows;
          this.pageFrom =
            this.oportunitiesRequest.pageNumber === 1
              ? 1
              : (this.oportunitiesRequest.pageNumber - 1) * 30 + 1;
          this.pageSize = this.pageFrom - 1 + res.pageRows;
        });
    }
  }

  changePage = (n: number) => {
    this.queryParams['page'] = (
      this.oportunitiesRequest.pageNumber + n
    ).toString();
    this.router.navigate(['/comprar'], { queryParams: this.queryParams });
  };
}
