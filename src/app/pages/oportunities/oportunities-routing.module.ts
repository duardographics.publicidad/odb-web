import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OportunitiesComponent } from './oportunities.component';

const routes: Routes = [{ path: '', component: OportunitiesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class OportunitiesRouting {}
