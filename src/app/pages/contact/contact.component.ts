import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ContactService } from 'src/app/shared/services/api/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  captcha = '';
  sending = false;
  sent = false;

  constructor(private contactService: ContactService, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('Contacta con nosotros | Oportunidades de Bancos');
  }

  name: FormControl = new FormControl('', [Validators.required]);
  email: FormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  tel: FormControl = new FormControl('');
  reason: FormControl = new FormControl('', [Validators.required]);
  comments: FormControl = new FormControl('');
  acceptance: FormControl = new FormControl('', [Validators.required]);

  contactForm = new FormGroup({
    name: this.name,
    email: this.email,
    tel: this.tel,
    reason: this.reason,
    comments: this.comments,
    acceptance: this.acceptance,
  });

  onSubmit() {
    if (this.contactForm.valid && !this.email.value.includes('.ru')) {
      this.sending = true;
      this.contactService
        .sendMail(this.contactForm.value, 'Formulario web')
        .subscribe({
          next: () => {
            this.sent = true;
            this.sending = false;
            this.contactForm.reset();
          },
        });
    }
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }
}
