import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WorkWithUsComponent } from './work-with-us.component';
import { WorkWithUsRouting } from './work-with-us-routing.module';
import { GsapDirectivesModule } from 'src/app/shared/directives/gsap/gsap-directives.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  declarations: [WorkWithUsComponent],
  imports: [
    CommonModule,
    RouterModule,
    WorkWithUsRouting,
    GsapDirectivesModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
  ],
  exports: [WorkWithUsComponent],
})
export class WorkWithUsModule {}
