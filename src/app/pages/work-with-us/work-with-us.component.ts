import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ContactService } from 'src/app/shared/services/api/contact.service';
import { FirebaseService } from 'src/app/shared/services/shared/firebase.service';

@Component({
  selector: 'app-work-with-us',
  templateUrl: './work-with-us.component.html',
  styleUrls: ['./work-with-us.component.scss'],
})
export class WorkWithUsComponent implements OnInit {
  constructor(
    private contactService: ContactService,
    private title: Title,
    private fireService: FirebaseService,
  ) {}

  ngOnInit(): void {
    this.title.setTitle('Trabaja con nosotros | Oportunidades de Bancos');
  }

  captcha = '';
  sending = false;
  sent = false;

  name: FormControl = new FormControl('', [Validators.required]);
  email: FormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  tel: FormControl = new FormControl('');
  zone: FormControl = new FormControl('', [Validators.required]);
  acceptance: FormControl = new FormControl('', [Validators.required]);

  contactForm = new FormGroup({
    name: this.name,
    email: this.email,
    tel: this.tel,
    zone: this.zone,
    acceptance: this.acceptance,
  });

  onSubmit() {
    if (this.contactForm.valid && !this.email.value.includes('.ru')) {
      const date = new Date();
      this.sending = true;
      this.fireService.addEntry({
        email: this.contactForm.value.email,
        movil: this.contactForm.value.tel,
        name: this.contactForm.value.name,
        zone: this.contactForm.value.zone,
        date: `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`,
      });
      this.contactService.sendMailFranchise(this.contactForm.value).subscribe({
        next: () => {
          this.sent = true;
        },
      });
      this.contactService
        .sendResponseFranchise(this.contactForm.value)
        .subscribe({
          next: () => {
            this.sent = true;
            this.sending = false;
          },
        });
    }
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }
}
