import { Tags } from './api/property.model';
import { Features } from './features.interface';

export interface Property {
  reference: string;
  oldReference: string;
  description: string;
  elevator: boolean;
  constructionYear: number;
  roomsNumber: number;
  bathroomsNumber: number;
  floorNumber: null;
  price: number;
  price2: number;
  price3: number;
  underRoofSqm: number;
  totalSqm: number;
  sqm1: number;
  sqm2: number;
  sqm3: number;
  sqm1Text: string;
  sqm2Text: string;
  sqm3Text: null;
  operationType: number;
  energyCertificate: string;
  dataSourceSiteUrl: string;
  zipCode: string;
  address: string;
  address2: null;
  latitude: number;
  longitude: number;
  municipality: Municipality;
  propertySubtype: PropertySubtype;
  dataSourceSite: DataSourceSite;
  propertyTags: Tags[];
  propertyMultimedias: PropertyMultimedia[];
  propertyFeatures: Features[];
  id: number;
  mainFeatures: MainFeatures;
  tags: string[];
}

export interface MainFeatures {
  constMts?: number;
  rooms?: number;
  baths?: number;
  age?: number;
}

export interface DataSourceSite {
  name: string;
  uri: string;
  logo: string;
  scriptAppPath: string;
  nextRun: Date;
  lastRun: Date;
  lastRunCompleted: Date;
  lastResult: string;
  id: number;
}

export interface Municipality {
  name: string;
  email: string;
  province: Municipality;
  id: number;
  country?: Municipality;
}

export interface PropertyMultimedia {
  name: Name;
  mediaType: number;
  description: string;
  uri: string;
  propertyId: number;
  id: number;
}

export enum Name {
  ImagenDeLaPropiedaed = 'Imagen de la propiedaed',
}

export interface PropertySubtype {
  propertyType: Municipality;
  name: string;
  id: number;
}
