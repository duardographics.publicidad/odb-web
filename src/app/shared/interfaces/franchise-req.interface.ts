export interface FranchiseeModel {
  email: string;
  movil: string;
  name: string;
  zone: string;
  date?: string;
}
