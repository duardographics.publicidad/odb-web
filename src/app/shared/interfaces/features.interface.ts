export interface Features {
  age?: string;
  subtype?: string;
  state?: string;
  orientation?: string;
  floor?: number | string;
  elevator?: boolean;
  storage?: boolean;
  garage?: boolean;
  energy?: null | string;
  underRoofSqm?: null | number;
}
