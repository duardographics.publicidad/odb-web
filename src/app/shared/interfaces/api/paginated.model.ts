export interface Paginated<T> {
  list: T[];
  pageNumber: number;
  pageSize: number;
  totalRows: number;
  pageRows: number;
}
