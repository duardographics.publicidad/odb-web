import { Features } from '../features.interface';

export interface PropertyModel {
  reference: string;
  oldReference: string;
  description: string;
  elevator: boolean;
  constructionYear: number;
  roomsNumber: number;
  bathroomsNumber: number;
  floorNumber: null;
  price: number;
  price2: number;
  price3: number;
  underRoofSqm: number;
  totalSqm: number;
  sqm1: number;
  sqm2: number;
  sqm3: number;
  sqm1Text: string;
  sqm2Text: string;
  sqm3Text: null;
  operationType: number;
  energyCertificate: string;
  dataSourceSiteUrl: string;
  zipCode: string;
  address: string;
  address2: null;
  latitude: number;
  longitude: number;
  municipality: Municipality;
  propertySubtype: PropertySubtype;
  dataSourceSite: DataSourceSite;
  propertyTags: Tags[];
  propertyMultimedias: PropertyMultimedia[];
  propertyFeatures: Features[];
  id: number;
  mainFeatures: MainFeatures;
  tags: string[];
}

export interface MainFeatures {
  constMts?: number;
  rooms?: number;
  baths?: number;
  age?: number;
}

export interface DataSourceSite {
  name: string;
  uri: string;
  logo: string;
  scriptAppPath: string;
  nextRun: Date;
  lastRun: Date;
  lastRunCompleted: Date;
  lastResult: string;
  id: number;
}

export interface Tags {
  id: number;
  tag: {
    id: number;
    name: string;
  };
  propertyID: number;
}

export interface Municipality {
  name: string;
  email?: string;
  province?: Province;
  id: number;
}

export interface Province {
  id: number;
  name: string;
  country?: {
    id: number;
    name: string;
    email?: string;
  };
  email?: string;
}

export interface PropertyMultimedia {
  name: Name;
  mediaType: number;
  description: string;
  uri: string;
  propertyId: number;
  id: number;
}

export enum Name {
  ImagenDeLaPropiedaed = 'Imagen de la propiedaed',
}

export interface PropertySubtype {
  propertyType?: PropertyType;
  name: string;
  id: number;
  email?: string;
}

export interface PropertyType {
  name: string;
  id: number;
  email?: string;
}
