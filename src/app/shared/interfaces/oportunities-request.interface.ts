export interface OportunitiesRequest {
  page: number;
  pageSize: number;
  municipalityID?: number;
  provinceID?: number;
  hasPropertyTypes?: { id: number }[];
  hasPropertySubTypes?: { id: number; propertyType: { id: number } }[];
}
