export interface MainFeaturesInterface {
  baths?: number;
  rooms?: number;
  age?: number;
  constMts?: number;
}
