export interface FilterInterface {
  provinceId?: number;
  municipalityID?: number;
  fromPrice?: number;
  toPrice?: number;
  fromSQM?: number;
  toSQM?: number;
  fromRoomsNumber?: number;
  toRoomsNumber?: number;
  fromBathroomsNumber?: number;
  toBathroomsNumber?: number;
  hasElevator?: boolean;
  hasMultimedia?: boolean;
  hasTags?: [
    {
      id: number;
    },
  ];
  hasPropertyTypes?: {
    id: number;
  }[];
  hasPropertySubTypes?: {
    id: number;
    propertyType?: {
      id: number;
    };
  }[];
  hasFeatures?: [
    {
      id?: number;
    },
  ];
  pageNumber: number;
  pageSize: number;
  sortDesc: boolean;
}
