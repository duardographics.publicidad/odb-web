import { Injectable } from '@angular/core';
import {
  addDoc,
  collectionData,
  CollectionReference,
  Firestore,
} from '@angular/fire/firestore';
import {
  collection,
  DocumentData,
  DocumentReference,
} from '@firebase/firestore';
import { Observable } from 'rxjs';
import { FranchiseeModel } from '../../interfaces/franchise-req.interface';

@Injectable({ providedIn: 'root' })
export class FirebaseService {
  workWithUsCollection!: CollectionReference<DocumentData>;

  constructor(private readonly firestore: Firestore) {
    this.workWithUsCollection = collection(this.firestore, 'work-with-us');
  }

  getAll(): Observable<any> {
    return collectionData(this.workWithUsCollection);
  }

  addEntry(
    franchisee: FranchiseeModel,
  ): Promise<DocumentReference<DocumentData>> {
    return addDoc(this.workWithUsCollection, franchisee);
  }
}
