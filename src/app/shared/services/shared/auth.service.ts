import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  sendEmailVerification,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut,
  User,
} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { LoginData } from '../../interfaces/login.interface';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private auth: Auth, private router: Router) {}

  user: User | null = null;
  verifiedUser = new Subject<boolean>();

  getUser(): Observable<User | null> {
    return of(this.auth.currentUser);
  }

  checkVerification() {
    return this.auth.onAuthStateChanged(user => {
      if (user) {
        if (user.emailVerified) {
          this.verifiedUser.next(true);
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    });
  }

  shouldReLogin(): boolean {
    if (window) {
      const lg = window.localStorage.getItem('--lg');
      const date = new Date().getDate();
      return this.user === null || lg !== date.toString();
    }
    return true;
  }

  login({ email, password }: LoginData) {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  register({ email, password }: LoginData) {
    return createUserWithEmailAndPassword(this.auth, email, password);
  }

  logOut() {
    window.localStorage.removeItem('--lg');
    return signOut(this.auth).then(() => this.router.navigate(['']));
  }

  resendVerificationEmail(): void {
    if (this.user) {
      sendEmailVerification(this.user).then(() =>
        alert(
          'Se ha enviado un enlace de verificación a tu correo electrónico',
        ),
      );
    }
  }

  resetPassword(email: string): void {
    sendPasswordResetEmail(this.auth, email)
      .then(() => {
        alert('Se ha enviado un enlace para restablecer tu contraseña');
      })
      .catch(() =>
        alert(
          'No se ha podido enviar el correo de restablecimiento de contraseña',
        ),
      );
  }
}
