import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MainFeatures, Property } from '../../interfaces/property';
import { PropertiesService } from '../api/properties.service';

@Injectable({
  providedIn: 'root',
})
export class PropertyService {
  public $property!: Property;

  private constructor(private apiProperty: PropertiesService) {}

  public getProperty(ref: string): Observable<Property> {
    return this.apiProperty.getPropertyByRef(ref).pipe(
      switchMap(res => {
        if (res) {
          const tags = [];
          const MainFeatures: MainFeatures = {};
          tags.push('Venta');
          tags.push('Nueva Oportunidad');

          if (res.roomsNumber != 0) {
            MainFeatures.rooms = res.roomsNumber;
          }
          if (res.bathroomsNumber != 0) {
            MainFeatures.baths = res.bathroomsNumber;
          }
          if (res.constructionYear != 0) {
            MainFeatures.age = new Date().getFullYear() - res.constructionYear;
          }
          if (res.totalSqm != 0) {
            MainFeatures.constMts = res.totalSqm;
          }

          res.tags = tags;
          res.mainFeatures = MainFeatures;

          res.oldReference = [...ref.replace('ODB_', '').replace('-HP', '')]
            .reverse()
            .join('');
          return of(res);
        }
        return of({} as Property);
      }),
    );
  }
}
