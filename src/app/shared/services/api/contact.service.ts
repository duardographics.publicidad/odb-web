import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import emails from '../features/data';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private http: HttpClient) {}

  URL = `${environment.serverBase}/sendEmail`;

  sendMail(form: any, type: string, options?: any): Observable<object> {
    let emailAddress = 'contacts@oportunidadesdebancos.es';
    if (options && options.zipcode) {
      if (emails[options.zipcode]) {
        emailAddress = emails[options.zipcode].mail;
      }
    }

    const body = {
      body: `<html><body>
      <h2>${type} - ${form.reason}</h2> 
      <br/> 
      <hr/> 
      <br/> 
      <h3>Nombre: ${form.name}</h3>
      <h3>Email de contacto: ${form.email}</h3>
      <h3>Teléfono de contacto: ${form.tel}</h3>
      <h3>Motivo de contacto: ${form.reason}</h3>
      ${
        type === 'Contacto Inmueble'
          ? '<h3>REF: ' + options.reference + '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble'
          ? '<h3>Subtipo: ' + options.subtype + '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble'
          ? '<h3>Ubicación: ' +
            options.province +
            ', ' +
            options.municipality +
            '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble'
          ? '<h3>C&oacute;digo Postal: ' + options.zipcode + '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble'
          ? '<h3>Precio: ' + options.price + '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble' && options.ogId
          ? '<h3>ID: ' + options.ogId + '</h3>'
          : ''
      }
      ${
        type === 'Contacto Inmueble' && options.site
          ? '<h3>Sitio de origen: ' + options.site + '</h3>'
          : ''
      }
      <h3>Comentarios: ${form.comments}</h3>
      <br/> 
      <hr/> 
      <br/> 
      <h6>Este es un mensaje automático y no es necesario responder.</h6>
      <img src="https://oportunidadesdebancos.es/assets/images/logo-odb-color.png" width="208" height="48" alt="Oportunidades de Bancos">
      </body></html>`,
      subject: `[${type} - Oportunidades de Bancos] - ${form.reason}`,
      email: emailAddress,
      //"email": `duardographics@gmail.com`,
      bodyIsHTML: true,
    };

    return this.http.post(this.URL, body);
  }

  sendMailFranchise(form: any): Observable<object> {
    const body = {
      body: `<html><body>
      <h2>Nueva solicitud - Trabaja con nosotros</h2> 
      <br/> 
      <hr/> 
      <br/> 
      <h3>Nombre: ${form.name}</h3>
      <h3>Email de contacto: ${form.email}</h3>
      <h3>Teléfono de contacto: ${form.tel}</h3>
      <h3>Provincia o zona: ${form.zone}</h3>
      <br/> 
      <hr/> 
      <br/> 
      <h6>Este es un mensaje automático y no es necesario responder.</h6>
      <img src="https://oportunidadesdebancos.es/assets/images/logo-odb-color.png" width="208" height="48" alt="Oportunidades de Bancos">
      <br/> 
      </body></html>`,
      subject: `[Nueva solicitud de Franquicia - Oportunidades de Bancos]`,
      email: 'franquicias@oportunidadesdebancos.es',
      // email: 'duardographics@gmail.com',
      bodyIsHTML: true,
    };
    return this.http.post(this.URL, body);
  }

  sendResponseFranchise(form: any): Observable<object> {
    const body = {
      body: `<html><body>
      <h2>Hola ${form.name},</h2> 
      <br/> 
      <p>Muchas gracias por interesarte en <strong>OPORTUNIDADES DE BANCOS</strong>.</p>
      <p>Accede a este enlace y conoce, con mayor detalle, todo lo que podemos ofrecerte para ser dueño de tu propio negocio inmobiliario: <a href="https://oportunidadesdebancos.es/masterclass">Masterclass Oportunidades de Bancos</a></p>
      <p>Muy pronto recibirás noticias nuestras.</p>
      <br/> 
      <p>Atentamente,</p>
      <img src="https://oportunidadesdebancos.es/assets/images/logo-odb-color.png" width="208" height="48" alt="Oportunidades de Bancos">
      <br/> 
      </body></html>`,
      subject: `Franquicia Oportunidades de Bancos`,
      email: form.email,
      // email: 'duardographics@gmail.com',
      bodyIsHTML: true,
    };
    return this.http.post(this.URL, body);
  }
}
