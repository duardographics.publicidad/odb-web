import { Injectable } from '@angular/core';
import { FOOTER_BASE } from '../../data/constant-data/footer-base';

@Injectable({
  providedIn: 'root',
})
export class FooterDataService {
  footerBase = FOOTER_BASE;

  getData(type: string) {
    let uriType = '1';
    if (type === 'Viviendas') {
      uriType = '1';
    }
    if (type === 'Pisos') {
      uriType = '1&subtype=2';
    }
    if (type === 'Chalets') {
      uriType = '1&subtype=4,5';
    }
    if (type === 'Terrenos') {
      uriType = '5';
    }
    if (type === 'Fincas Rústicas') {
      uriType = '1,5&subtype=6,7';
    }
    if (type === 'Garajes') {
      uriType = '6&subtype=28';
    }
    if (type === 'Locales') {
      uriType = '6&subtype=10';
    }
    if (type === 'Naves') {
      uriType = '4&subtype=19,20,21';
    }

    const data = this.footerBase.map(x => {
      const obj = { name: type + x.name, url: `${x.url}&type=${uriType}` };
      return obj;
    });

    return data;
  }
}
