import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FilterInterface } from '../../interfaces/filter.interface';
import { Paginated } from '../../interfaces/api/paginated.model';
import { PropertyModel } from '../../interfaces/api/property.model';
import { Property } from '../../interfaces/property';
import { environment } from 'src/environments/environment';

const headers: HttpHeaders = new HttpHeaders();
headers.set('SecurityKey', environment.apiKey);
@Injectable({
  providedIn: 'root',
})
export class PropertiesService {
  constructor(private http: HttpClient) {}

  getPropertiesHome(): Observable<Paginated<PropertyModel>> {
    const body = {
      pageSize: 10,
      pageNumber: 1,
      fromPrice: 50000,
      hasPropertyTypes: [{ id: 1 }, { id: 5 }, { id: 2 }],
      sortDesc: true,
      hasMultimedia: true,
    };
    return this.http
      .post('/api/searchProperties', body)
      .pipe(map(r => r as Paginated<PropertyModel>));
  }

  getPropertiesMunicipality(
    municipality: number,
    pageSize?: number,
    pageNumber?: number,
  ): Observable<Paginated<PropertyModel>> {
    const body = {
      pageSize: pageSize,
      pageNumber: pageNumber,
      municipalityID: municipality,
      sortDesc: true,
      hasMultimedia: true,
    };
    return this.http
      .post('/api/searchProperties', body)
      .pipe(map(m => m as Paginated<PropertyModel>));
  }

  getPropertiesProvince(
    province: number,
    pageSize?: number,
    pageNumber?: number,
  ): Observable<Paginated<PropertyModel>> {
    const body = {
      pageSize: pageSize,
      pageNumber: pageNumber,
      provinceID: province,
      sortDesc: true,
      hasMultimedia: true,
    };
    return this.http
      .post('/api/searchProperties', body)
      .pipe(map(m => m as Paginated<PropertyModel>));
  }

  getPropertyByRef(ref: string) {
    const body = { reference: ref };
    return this.http
      .post('/api/getProperty', body)
      .pipe(map(m => m as Property));
  }

  loadOportunitiesPage(
    requestOptions: FilterInterface,
  ): Observable<Paginated<PropertyModel>> {
    let body = {
      pageSize: requestOptions.pageSize,
      pageNumber: requestOptions.pageNumber,
      sortDesc: true,
    };
    requestOptions.hasMultimedia = true;
    body = requestOptions;
    return this.http
      .post('/api/searchProperties', body)
      .pipe(map(r => r as Paginated<PropertyModel>));
  }

  disableProperty(id: number): Observable<string> {
    return this.http.put(
      `${environment.apiBase}/Properties/Disable/${id}`,
      '',
      { headers: { SecurityKey: environment.apiKey } },
    ) as Observable<string>;
  }
  enableProperty(id: number): Observable<string> {
    return this.http.put(`${environment.apiBase}/Properties/Enable/${id}`, '', {
      headers: { SecurityKey: environment.apiKey },
    }) as Observable<string>;
  }

  deleteProperty(id: number): Observable<ArrayBuffer> {
    return this.http.delete(`${environment.apiBase}/Properties/${id}`, {
      headers: { SecurityKey: environment.apiKey },
    }) as Observable<ArrayBuffer>;
  }
}
