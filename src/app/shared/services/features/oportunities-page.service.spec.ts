import { TestBed } from '@angular/core/testing';

import { OportunitiesPageService } from './oportunities-page.service';

describe('OportunitiesPageService', () => {
  let service: OportunitiesPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OportunitiesPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
