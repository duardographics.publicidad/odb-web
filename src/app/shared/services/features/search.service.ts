import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PARTNERS_LINKS } from '../../data/constant-data/partners-links';
import { COMMON_SEARCHES } from '../../data/constant-data/common-searches';
import {
  Municipality,
  PropertyModel,
  Province,
} from '../../interfaces/api/property.model';
import { Paginated } from '../../interfaces/api/paginated.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  commonSearches = COMMON_SEARCHES;

  partnersLinks = PARTNERS_LINKS;

  constructor(private http: HttpClient) {}

  getProvincesMatches(word: string): Observable<Paginated<Province>> {
    const URL = '/api/getProvince';
    return this.http
      .post(URL, { word: word })
      .pipe(map(r => r as Paginated<Province>));
  }

  getProvinceByID(id: number): Observable<Province> {
    const URL = '/api/getProvinceByID';
    return this.http.post(URL, { id: id }).pipe(map(m => m as Province));
  }

  getMunicipalitiesMatches(word: string): Observable<Paginated<Municipality>> {
    const URL = '/api/getMunicipality';
    return this.http
      .post(URL, { word: word })
      .pipe(map(r => r as Paginated<Municipality>));
  }

  getMunicipalitiesByID(id: number): Observable<Municipality> {
    const URL = '/api/getMunicipalityByID';
    return this.http.post(URL, { id: id }).pipe(map(r => r as Municipality));
  }

  getPropertyByRef(ref: string): Observable<PropertyModel> {
    const URL = '/api/getProperty';
    return this.http.post(URL, { reference: ref }) as Observable<PropertyModel>;
  }
}
