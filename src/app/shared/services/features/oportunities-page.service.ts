import { Injectable } from '@angular/core';
import { COMMON_SEARCHES } from '../../data/constant-data/common-searches';

@Injectable({
  providedIn: 'root',
})
export class OportunitiesPageService {
  commonSearches = COMMON_SEARCHES;
}
