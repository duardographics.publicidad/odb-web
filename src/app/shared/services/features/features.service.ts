import { Injectable } from '@angular/core';
import { Features } from '../../interfaces/features.interface';
import { Property } from '../../interfaces/property';

@Injectable({
  providedIn: 'root',
})
export class FeaturesService {
  getFeatures(property: Property) {
    const features: Features = {};

    if (property.constructionYear > 0) {
      const age = property.constructionYear;
      if (age < 5) {
        features.age = 'Menos de 5 años';
      } else if (age < 10) {
        features.age = '5 a 10 años';
      } else if (age < 20) {
        features.age = '10 a 20 años';
      } else if (age < 50) {
        features.age = '20 a 50 años';
      } else if (age < 70) {
        features.age = '50 a 70 años';
      } else if (age < 100) {
        features.age = '70 a 100 años';
      } else {
        features.age = 'Más de 100 años';
      }
    }

    features.subtype = property.propertySubtype.name;

    if (property.energyCertificate != null) {
      features.energy = property.energyCertificate;
    }
    if (property.elevator != false) {
      features.elevator = true;
    }
    if (property.underRoofSqm > 0) {
      features.underRoofSqm = property.underRoofSqm;
    }

    return features;
  }
}
