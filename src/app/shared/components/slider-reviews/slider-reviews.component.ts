import { Component, Input } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-slider-reviews',
  templateUrl: './slider-reviews.component.html',
  styleUrls: ['./slider-reviews.component.scss'],
})
export class SliderReviewsComponent {
  @Input() reviewsVideos?: boolean;
  @Input() reviewsVideosProf?: boolean;
  @Input() reviewsGoogle?: boolean;

  reviewVideos: string[] = [
    '/assets/videos/testimonio2-1.mp4',
    '/assets/videos/testimonio-2-2.mp4',
    '/assets/videos/testimonio-2-3.mp4',
    '/assets/videos/testimonio-2-4.mp4',
    '/assets/videos/testimonio-2-5.mp4',
    '/assets/videos/testimonio-2-6.mp4',
  ];
  reviewVideosPro: string[] = [
    '/assets/videos/testimonio-1-1.mp4',
    '/assets/videos/testimonio-1-2.mp4',
    '/assets/videos/testimonio-1-3.mp4',
    '/assets/videos/testimonio-1-4.mp4',
    '/assets/videos/testimonio-1-5.mp4',
    '/assets/videos/testimonio-1-6.mp4',
    '/assets/videos/testimonio-1-7.mp4',
    '/assets/videos/testimonio-1-8.mp4',
  ];
  reviewGoogle: string[] = [
    '/assets/images/odb-reviews-1.png',
    '/assets/images/odb-reviews-2.png',
    '/assets/images/odb-reviews-3.png',
    '/assets/images/odb-reviews-4.png',
    '/assets/images/odb-reviews-5.png',
    '/assets/images/odb-reviews-6.png',
  ];

  videoOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    margin: 0,
    dots: true,
    navSpeed: 300,
    navText: [
      '<i class="bi bi-chevron-left"></i>',
      '<i class="bi bi-chevron-right"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
    },
    nav: false,
  };
  googleOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    margin: 20,
    dots: true,
    navSpeed: 300,
    navText: [
      '<i class="bi bi-chevron-left"></i>',
      '<i class="bi bi-chevron-right"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      760: {
        items: 2,
      },
      1440: {
        items: 3,
      },
    },
    nav: false,
  };
}
