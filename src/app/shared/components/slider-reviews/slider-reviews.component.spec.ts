import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderReviewsComponent } from './slider-reviews.component';

describe('SliderReviewsComponent', () => {
  let component: SliderReviewsComponent;
  let fixture: ComponentFixture<SliderReviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SliderReviewsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SliderReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
