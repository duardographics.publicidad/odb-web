import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  Inject,
  InjectionToken,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { Router } from '@angular/router';
import { PropertyModel, Province } from '../../interfaces/api/property.model';
import { Municipality } from '../../interfaces/property';
import { SearchService } from '../../services/features/search.service';

@Component({
  selector: 'app-search-home',
  templateUrl: './search-home.component.html',
  styleUrls: ['./search-home.component.scss'],
})
export class SearchHomeComponent implements OnInit {
  searchByLocation = true;
  provinces: Province[] = [];
  municipalities: Municipality[] = [];
  reference: PropertyModel | null = null;
  refError = false;
  propertyType = 0;
  searching = false;

  constructor(
    private search: SearchService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      window.addEventListener('click', (e: any) => {
        if (e.path && e.path[0]) {
          const path = e.path[0].nodeName;
          if (path === 'SECTION' || path === 'DIV') {
            this.clearSearch();
          }
        }
      });
    }
  }

  normalSearch = (): void => {
    this.searchByLocation = true;
  };
  refSearch = (): void => {
    this.searchByLocation = false;
  };

  clearSearch = (): void => {
    this.provinces = [];
    this.municipalities = [];
    this.reference = null;
    this.refError = false;
  };

  toggleSearchTab = (value: boolean): void => {
    this.searchByLocation = value;
  };

  searchByWords = (inputSearch: string): void => {
    inputSearch = inputSearch.trim();
    this.clearSearch();
    if (inputSearch.length >= 3) {
      this.getResults(inputSearch);
    } else {
      this.clearSearch();
    }
  };

  getResults = (inputSearch: string) => {
    this.searching = true;
    if (this.searchByLocation) {
      this.search.getMunicipalitiesMatches(inputSearch).subscribe((x: any) => {
        this.municipalities = x.list;
        this.searching = false;
      });
      this.search.getProvincesMatches(inputSearch).subscribe((x: any) => {
        this.provinces = x.list;
        this.searching = false;
      });
    } else {
      this.search.getPropertyByRef(inputSearch).subscribe({
        next: x => (this.reference = x),
        error: () => (this.refError = true),
      });
    }
  };

  clickOnResults(type: string, id: string) {
    if (this.searchByLocation) {
      const params: any = {};
      params.page = 1;

      if (type === 'province') {
        params.provinceID = Number(id);
      } else if (type === 'municipality') {
        params.municipalityID = Number(id);
      }
      if (this.propertyType != 0) {
        params.type = this.propertyType;
      }
      this.router.navigate(['/comprar'], { queryParams: params });
    } else {
      this.router.navigate(['/inmueble/' + id]);
    }
  }

  searchPropByRef(ref: string) {
    this.search.getPropertyByRef(ref).subscribe({
      next: (x: PropertyModel) => {
        this.reference = x;
        this.searching = false;
      },
    });
  }

  buyByLocation = () => {
    const params: any = {};
    params.page = 1;
    if (this.provinces.length > 0) {
      params.provinceID = this.provinces[0].id;
    } else if (this.municipalities.length > 0) {
      params.municipalityID = this.municipalities[0].id;
    }
    if (this.propertyType != 0) {
      params.type = this.propertyType;
    }
    this.router.navigate(['/comprar'], { queryParams: params });
  };

  changeType(id: string) {
    this.propertyType = Number(id);
  }
}
