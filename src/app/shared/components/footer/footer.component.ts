import { Component, Input, OnInit } from '@angular/core';
import { FooterDataService } from '../../services/api/footerData.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() isLanding = false;

  constructor(private footerData: FooterDataService) {}

  selectedTab = 'Viviendas';
  footerSearchLinks?: { name: string; url: string }[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.footerSearchLinks = this.footerData.getData(this.selectedTab);
  }

  changeTabs(type: string) {
    this.selectedTab = type;
    this.getData();
  }
}
