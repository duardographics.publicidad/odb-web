import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './slider/slider.component';
import { NavHomeComponent } from './header/nav-home/nav-home.component';
import { NavInsideComponent } from './header/nav-inside/nav-inside.component';
import { SearchHomeComponent } from './search-home/search-home.component';
import { RouterModule } from '@angular/router';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { NavLandingComponent } from './header/nav-landing/nav-landing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SliderReviewsComponent } from './slider-reviews/slider-reviews.component';
import { RecaptchaModule } from 'ng-recaptcha';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    NavHomeComponent,
    NavInsideComponent,
    SearchHomeComponent,
    ContactFormComponent,
    NavLandingComponent,
    SliderReviewsComponent,
  ],
  imports: [
    CommonModule,
    CarouselModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    SearchHomeComponent,
    FooterComponent,
    ContactFormComponent,
    SliderReviewsComponent,
  ],
})
export class ComponentsModule {}
