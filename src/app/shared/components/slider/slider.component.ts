import { isPlatformBrowser } from '@angular/common';
import {
  Component,
  Inject,
  InjectionToken,
  Input,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Paginated } from '../../interfaces/api/paginated.model';
import { PropertyModel } from '../../interfaces/api/property.model';
import { PropertiesService } from '../../services/api/properties.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  properties: PropertyModel[] = [];
  @Input() search = '';

  constructor(
    private propService: PropertiesService,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      if (this.search === 'home') {
        this.propService.getPropertiesHome().subscribe({
          next: (res: Paginated<PropertyModel>) => {
            this.properties = res.list;
          },
        });
      } else if (this.search != undefined) {
        this.propService
          .getPropertiesProvince(Number(this.search), 10, 1)
          .subscribe({
            next: (res: Paginated<PropertyModel>) => {
              this.properties = res.list;
            },
          });
      }
    }
  }

  propertiesOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    margin: 20,
    dots: true,
    navSpeed: 300,
    navText: [
      '<i class="bi bi-chevron-left"></i>',
      '<i class="bi bi-chevron-right"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      760: {
        items: 3,
      },
      1024: {
        items: 4,
      },
      1440: {
        items: 5,
      },
    },
    nav: true,
  };
}
