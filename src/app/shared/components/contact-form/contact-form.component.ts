import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Property } from '../../interfaces/property';
import { ContactService } from '../../services/api/contact.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent {
  @Input() property!: Property;

  constructor(private contactService: ContactService) {}

  captcha = '';
  sending = false;
  sent = false;

  name: FormControl = new FormControl('', [Validators.required]);
  email: FormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  tel: FormControl = new FormControl('');
  reason: FormControl = new FormControl('', [Validators.required]);
  comments: FormControl = new FormControl('');
  acceptance: FormControl = new FormControl('', [Validators.required]);

  contactForm = new FormGroup({
    name: this.name,
    email: this.email,
    tel: this.tel,
    reason: this.reason,
    comments: this.comments,
    acceptance: this.acceptance,
  });

  onSubmit() {
    if (this.contactForm.valid && !this.email.value.includes('.ru')) {
      const options = {
        reference: this.property.reference,
        ogId: this.property.oldReference,
        site:
          this.property.dataSourceSiteUrl ?? this.property.dataSourceSite.name,
        province: this.property.municipality.province.name,
        municipality: this.property.municipality.name,
        zipcode: this.property.zipCode,
        price: this.property.price,
        subtype: this.property.propertySubtype.name,
      };

      this.sending = true;
      this.contactService
        .sendMail(this.contactForm.value, 'Contacto Inmueble', options)
        .subscribe({
          next: () => {
            this.sent = true;
            this.sending = false;
          },
        });
    }
  }

  resolved(captchaResponse: string) {
    this.captcha = captchaResponse;
  }
}
