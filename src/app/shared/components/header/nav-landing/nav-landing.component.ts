import { Component } from '@angular/core';

@Component({
  selector: 'app-nav-landing',
  templateUrl: './nav-landing.component.html',
  styleUrls: ['./nav-landing.component.scss'],
})
export class NavLandingComponent {}
