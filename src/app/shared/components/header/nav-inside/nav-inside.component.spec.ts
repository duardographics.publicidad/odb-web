import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavInsideComponent } from './nav-inside.component';

describe('NavInsideComponent', () => {
  let component: NavInsideComponent;
  let fixture: ComponentFixture<NavInsideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavInsideComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NavInsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
