import { Component } from '@angular/core';

@Component({
  selector: 'app-nav-inside',
  templateUrl: './nav-inside.component.html',
  styleUrls: ['./nav-inside.component.scss'],
})
export class NavInsideComponent {
  openMenu = false;

  toggleMenu = (): void => {
    this.openMenu = !this.openMenu;
  };
}
