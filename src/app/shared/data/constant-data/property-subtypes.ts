import { PropertySubtype } from '../../interfaces/api/property.model';

export const PROPERTY_SUBTYPES: PropertySubtype[] = [
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Casa',
    id: 1,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Piso',
    id: 2,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Dúplex',
    id: 3,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Chalet adosado',
    id: 4,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Chalet independiente',
    id: 5,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Casa rural',
    id: 6,
  },
  {
    propertyType: {
      name: 'Viviendas y pisos',
      id: 1,
    },
    name: 'Casa de pueblo',
    id: 7,
  },
  {
    propertyType: {
      name: 'Locales comerciales',
      id: 2,
    },
    name: 'Almacén',
    id: 8,
  },
  {
    propertyType: {
      name: 'Locales comerciales',
      id: 2,
    },
    name: 'Gasolinera',
    id: 9,
  },
  {
    propertyType: {
      name: 'Locales comerciales',
      id: 2,
    },
    name: 'Local',
    id: 10,
  },
  {
    propertyType: {
      name: 'Locales comerciales',
      id: 2,
    },
    name: 'Oficina',
    id: 11,
  },
  {
    propertyType: {
      name: 'Locales comerciales',
      id: 2,
    },
    name: 'Parque medianas',
    id: 12,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Aparcamiento',
    id: 13,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Edificio',
    id: 14,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Edificio comercial',
    id: 15,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Edificio turístico',
    id: 16,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Edificio de oficinas',
    id: 17,
  },
  {
    propertyType: {
      name: 'Edificios',
      id: 3,
    },
    name: 'Edificio residencial',
    id: 18,
  },
  {
    propertyType: {
      name: 'Industrial',
      id: 4,
    },
    name: 'Nave adosada',
    id: 19,
  },
  {
    propertyType: {
      name: 'Industrial',
      id: 4,
    },
    name: 'Nave aislada',
    id: 20,
  },
  {
    propertyType: {
      name: 'Industrial',
      id: 4,
    },
    name: 'Nave en varias plantas',
    id: 21,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'No urbanizable',
    id: 22,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'Suelo rústico no urbanizable',
    id: 23,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'Suelo urbano no consolidado',
    id: 24,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'Urbanizable no programado',
    id: 25,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'Urbanizable programado',
    id: 26,
  },
  {
    propertyType: {
      name: 'Suelos',
      id: 5,
    },
    name: 'Solar',
    id: 27,
  },
  {
    propertyType: {
      name: 'Garajes y trasteros',
      id: 6,
    },
    name: 'Garaje',
    id: 28,
  },
  {
    propertyType: {
      name: 'Garajes y trasteros',
      id: 6,
    },
    name: 'Trastero',
    id: 29,
  },
  {
    propertyType: {
      name: 'Hotel',
      id: 7,
    },
    name: 'Hotel',
    id: 30,
  },
  {
    propertyType: {
      name: 'Obra parada',
      id: 8,
    },
    name: 'Obra parada',
    id: 31,
  },
  {
    propertyType: {
      name: 'Otros',
      id: 9,
    },
    name: 'Otro',
    id: 32,
  },
];
