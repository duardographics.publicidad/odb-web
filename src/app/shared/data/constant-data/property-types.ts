import { PropertyType } from '../../interfaces/api/property.model';

export const PROPERTY_TYPES: PropertyType[] = [
  {
    name: 'Viviendas y pisos',
    id: 1,
  },
  {
    name: 'Locales comerciales',
    id: 2,
  },
  {
    name: 'Edificios',
    id: 3,
  },
  {
    name: 'Industrial',
    id: 4,
  },
  {
    name: 'Suelos',
    id: 5,
  },
  {
    name: 'Garajes y trasteros',
    id: 6,
  },
  {
    name: 'Otros',
    id: 7,
  },
];
