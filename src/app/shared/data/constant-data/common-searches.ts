export const COMMON_SEARCHES = [
  {
    location: 'Madrid',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=4570',
    typeOf: 'piso',
    bacgkround: '/assets/images/madrid-small.jpg',
  },
  {
    location: 'Barcelona',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=1053',
    typeOf: 'piso',
    bacgkround: '/assets/images/barcelona-small.jpg',
  },
  {
    location: 'Valencia',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=7222',
    typeOf: 'piso',
    bacgkround: '/assets/images/valencia-small.jpg',
  },
  {
    location: 'Zaragoza',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=8112',
    typeOf: 'piso',
    bacgkround: '/assets/images/zaragoza-small.jpg',
  },
  {
    location: 'Gijón',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=500',
    typeOf: 'piso',
    bacgkround: '/assets/images/gijon-small.jpg',
  },
  {
    location: 'Málaga',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=4738',
    typeOf: 'piso',
    bacgkround: '/assets/images/malaga-small.jpg',
  },
  {
    location: 'A Coruña',
    url: '/comprar?page=1&type=1&subtype=2&provinceID=1',
    typeOf: 'piso',
    bacgkround: '/assets/images/coruña-small.jpg',
  },
  {
    location: 'Bilbao',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=7483',
    typeOf: 'piso',
    bacgkround: '/assets/images/bilbao-small.jpg',
  },
  {
    location: 'Alicante',
    url: '/comprar?page=1&type=1&subtype=2&municipalityID=246',
    typeOf: 'piso',
    bacgkround: '/assets/images/alicante-small.jpg',
  },
];
