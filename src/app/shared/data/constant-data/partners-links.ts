export const PARTNERS_LINKS = [
  {
    url: 'https://www.bbva.es/personas.html',
    img: '1',
  },
  {
    url: 'https://www.bancosantander.es/particulares',
    img: '2',
  },
  {
    url: 'https://www.haya.es/',
    img: '3',
  },
  {
    url: 'https://www.caixabank.es/particular/home/particulares_es.html',
    img: '4',
  },
  {
    url: 'https://www.alisedainmobiliaria.com/',
    img: '5',
  },
  {
    url: 'https://www.casaktua.com/',
    img: '6',
  },
  {
    url: 'https://www.bbva.es/personas.html',
    img: '7',
  },
  {
    url: 'https://www.escogecasa.es/',
    img: '8',
  },
  {
    url: 'https://www.bankinter.com/banca/inicio',
    img: '9',
  },
  {
    url: 'https://www.holapisos.com/',
    img: '10',
  },
  {
    url: 'https://www.retamarealestate.com/',
    img: '11',
  },
  {
    url: 'https://portalnow.com/es/',
    img: '12',
  },
  {
    url: 'https://www.bancamarch.es/es/',
    img: '13',
  },
  {
    url: 'https://www.nolon.es/es/',
    img: '14',
  },
  {
    url: 'https://uci.com/',
    img: '15',
  },
  {
    url: 'https://www.abanca.com/',
    img: '16',
  },
  {
    url: 'https://www.sareb.es/',
    img: '17',
  },
  // {
  //   url:'https://www.servihabitat.com/',
  //   img:'18'
  // },
  {
    url: 'https://www.ruralvia.com/',
    img: '19',
  },
  {
    url: 'https://digloservicer.com/',
    img: '20',
  },
  {
    url: 'https://www.solvia.es/',
    img: '21',
  },
  {
    url: 'https://www.liberbank.es/',
    img: '22',
  },
  {
    url: 'https://www.altamirainmuebles.com/venta-pisos',
    img: '23',
  },
  {
    url: 'https://www.cajamar.es/es/comun/',
    img: '24',
  },
  {
    url: 'https://www.caixabank.es/particular/general/caixabank-bankia.html',
    img: '25',
  },
  {
    url: 'https://www.unicajabanco.es/es/particulares',
    img: '26',
  },
  {
    url: 'https://www.unicajabanco.es/es/particulares',
    img: '27',
  },
  {
    url: 'https://www.neinorhomes.com/',
    img: '28',
  },
  {
    url: 'https://www.ibercaja.es/',
    img: '29',
  },
  {
    url: 'https://www.goldmansachs.com/worldwide/spain/',
    img: '30',
  },
  {
    url: 'https://www.caixabank.es/particular/home/particulares_es.html',
    img: '31',
  },
  {
    url: 'https://www.alisedainmobiliaria.com/',
    img: '32',
  },
  {
    url: 'https://www.anticipa.com/corp/',
    img: '33',
  },
  {
    url: 'https://www.hipoges.com/',
    img: '34',
  },
];
