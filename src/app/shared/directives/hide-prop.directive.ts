import { Directive } from '@angular/core';

@Directive({
  selector: '[appHideProp]',
})
export class HidePropDirective {}
