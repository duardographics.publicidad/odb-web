import { isPlatformBrowser } from '@angular/common';
import {
  Directive,
  OnInit,
  ElementRef,
  Input,
  Inject,
  PLATFORM_ID,
  InjectionToken,
} from '@angular/core';
import gsap from 'gsap';

@Directive({
  selector: '[appFadeIn]',
})
export class FadeInDirective implements OnInit {
  @Input() duration = 1;
  @Input() delay = 0;

  protected timeline: any;

  constructor(
    protected element: ElementRef,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<unknown>,
  ) {
    if (isPlatformBrowser(platformId)) {
      this.timeline = gsap.timeline();
    }
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.animateIn();
    }
  }

  protected animateIn() {
    if (isPlatformBrowser(this.platformId)) {
      this.timeline.from(
        this.element.nativeElement,
        this.duration,
        { opacity: '0', y: 50, ease: 'Expo.easeInOut' },
        this.delay,
      );
    }
  }
}
