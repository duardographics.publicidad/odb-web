# Changelog

## v.2.0.0

### Features

- Adding Husky and configuring it with ESlint + Prettier to run prehooks.
